﻿using DocumentFormat.OpenXml.Drawing;
using SharpGL.SceneGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private enum Shape
        {
            Line, Ellipse, Triangle, Circle, Rectangle
        }

        private object _currentShape;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void LineButton_Click(object sender, RoutedEventArgs e)
        {
            _currentShape = Shape.Line;
            //testText.Text = "LINE !!!";
        }
        private void EllipseButton_Click(object sender, RoutedEventArgs e)
        {
            _currentShape = Shape.Ellipse;
            //testText.Text = "Ellipse";
        }
        private void TriangleButton_Click(object sender, RoutedEventArgs e)
        {
            _currentShape = Shape.Triangle;
            //testText.Text = "TRIANGLE !!!";
        }
        private void CircleButton_Click(object sender, RoutedEventArgs e)
        {
            _currentShape = Shape.Circle;
            //testText.Text = "круг";
        }
        private void RectangleButton_Click(object sender, RoutedEventArgs e)
        {
            _currentShape = Shape.Rectangle;
            //testText.Text = "прямоугольник";
        }

        //Функции меню
        private void menuOpen_Click(object sender, RoutedEventArgs e)
        {
            
        }
        private void menuSave_Click(object sender, RoutedEventArgs e)
        {

        }
        private void menuSaveAs_Click(object sender, RoutedEventArgs e)
        {
            
        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void menuUndo_Click(object sender, RoutedEventArgs e)
        {
            // Поместите свою функцию отмены действия
        }
        private void menuCopy_Click(object sender, RoutedEventArgs e)
        {
            // Поместите свою функцию копирования
        }
        private void menuPaste_Click(object sender, RoutedEventArgs e)
        {
            // Поместите свою функцию отмены вставки
        }
        private void menuCut_Click(object sender, RoutedEventArgs e)
        {
            // Поместите свою функцию отмены вырезки
        }


        private void clrPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            var red_color = myColorPicker.SelectedColor.Value.R;
            var blue_color = myColorPicker.SelectedColor.Value.B;
            var green_color = myColorPicker.SelectedColor.Value.G;
            var alpha = myColorPicker.SelectedColor.Value.A;
        }

        private void SelectThickNess_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var StrokeSize = 0;
            switch (SelectThickNess.SelectedIndex)
            {
                case 0:
                    StrokeSize = 1;
                    break;
                case 1:
                    StrokeSize = 2;
                    break;
                case 2:
                    StrokeSize = 3;
                    break;
                default:
                    break;
            }
        }


        private void btnEraser_Click(object sender, RoutedEventArgs e)
        {
            if (btnEraser.IsChecked.Value)
            {
                var sizelastik = 20;
                inkPanel.EditingMode = InkCanvasEditingMode.EraseByPoint;
                inkPanel.EraserShape = new EllipseStylusShape(sizelastik, sizelastik);
            }
            else
            {
                inkPanel.EditingMode = InkCanvasEditingMode.Ink;
            }
        }


        //Opengl

        //Graphic graphic;
        //Size OpenGLControl_size;
        //ViewModel.ViewModel viewModel = new ViewModel.ViewModel();
        //private void OpenGLControl_OpenGLInitialized(object sender, OpenGLEventArgs args)
        //{
        //    graphic = new Graphic((IEnumerable<DocumentFormat.OpenXml.OpenXmlElement>)args);
        //}

        //private void OpenGLControl_OpenGLDraw(object sender, OpenGLEventArgs args)
        //{
        //    viewModel.Draw.Execute(graphic).Subscribe();
        //}

        //private void OpenGLControl_Resized(object sender, OpenGLEventArgs args)
        //{
        //    var gl = args.OpenGL;
        //    gl.LoadIdentity();
        //    gl.Ortho2D(0, OpenGLControl_size.Width, 0, OpenGLControl_size.Height);
        //}

        //private void OpenGLControl_SizeChanged(object sender, SizeChangedEventArgs size)
        //{
        //    OpenGLControl_size = size.NewSize;
        //}

        //private void OpenGLControl_MouseDown(object sender, MouseEventArgs e)
        //{
        //    var SelectedObject = e.Source as UIElement;
        //    Point pos = e.GetPosition(SelectedObject);
        //    ViewModel.MouseDown.Execute((new DV_Model.Point(pos.X, OpenGLControl_size.Height - pos.Y), new DV_Model.Pen(curPenColor, curWidth))).Subscribe();
        //}

        //private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        //{
        //    var SelectedObject = e.Source as UIElement;
        //    Point pos = e.GetPosition(SelectedObject);
        //    ViewModel.MouseMove.Execute(new DV_Model.Point(pos.X, OpenGLControl_size.Height - pos.Y)).Subscribe();
        //}

        //private void OpenGLControl_MouseUp(object sender, MouseButtonEventArgs e)
        //{
        //    var SelectedObject = e.Source as UIElement;
        //    Point pos = e.GetPosition(SelectedObject);
        //    ViewModel.MouseUp.Execute(new DV_Model.Point(pos.X, OpenGLControl_size.Height - pos.Y)).Subscribe();


        //    //Тут сделать и другие клавиши
        //    Line.IsChecked = false;
        //    Ellipse.IsChecked = false;
        //    Rectangle.IsChecked = false;
        //}

        //Тестовые функции
    }
}
